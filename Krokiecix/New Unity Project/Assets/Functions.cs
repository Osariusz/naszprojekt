﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Functions : MonoBehaviour
{
    static ImpScript importantScript = Statics.ImportantScript;

    public static void ObtainObject(int id,GameObject item)
    {
        item.GetComponentInChildren<EqButton>().ItemPresent = true;
        item.GetComponentInChildren<SpriteRenderer>().enabled = true;
    }
    public static void ThrowObject(int id, GameObject item)
    {
        item.GetComponentInChildren<EqButton>().ItemPresent = false;
        item.GetComponentInChildren<SpriteRenderer>().enabled = false;
    }
    public static void ToogleObjectStance(GameObject Gobject)
    {
        Gobject.GetComponentInChildren<SpriteRenderer>().enabled = !Gobject.GetComponentInChildren<Object>().enabled;
        Gobject.GetComponentInChildren<BoxCollider2D>().enabled = !Gobject.GetComponentInChildren<BoxCollider2D>().enabled;
        Gobject.GetComponentInChildren<Object>().enabled = !Gobject.GetComponentInChildren<Object>().enabled;
    }
    public static void UnlockAchiev(string id)
    {
        GameObject[] toDelete = GameObject.FindGameObjectsWithTag("AText");
        foreach(GameObject item in toDelete)
        {
            Destroy(item);
        }

        Text[] texts = importantScript.Book.GetComponentInChildren<Canvas>().GetComponentsInChildren<Text>();
        Text achievCompleted = null;
        foreach (Text item in texts)
        {
            if (item.name == id)
            {
                achievCompleted = item;
                if(achievCompleted.GetComponentInChildren<AchievData>().Done == false)
                {
                    Text newAchiev;
                    newAchiev = Instantiate(item, new Vector3(592f, 55f, -10f), Statics.ImportantScript.MainCamera.transform.rotation);
                    newAchiev.tag = "AText";
                    newAchiev.transform.SetParent(Statics.ImportantScript.NotPoradnik.transform);
                    newAchiev.transform.localScale = new Vector3(2f, 2f, 2f);
                    importantScript.AchievDelete = 0.1f;
                }

            }
        }
        
        if(achievCompleted.GetComponentInChildren<AchievData>().Done == false)
        {
            achievCompleted.GetComponentInChildren<AchievData>().Done = true;
            importantScript.Book.SetActive(true);
            AudioSource achiev = importantScript.MainCamera.GetComponentInChildren<AudioSource>();
            achiev.Play(0);

            float ourWidth = 0;

            if(achievCompleted.preferredWidth > 200)
            {
                ourWidth = 200;
            }
            else
            {
                ourWidth = achievCompleted.preferredWidth;
            }

            Button newFaja = Instantiate(importantScript.faja, new Vector3(achievCompleted.transform.position.x - ourWidth/2 - 130, achievCompleted.transform.position.y +10, achievCompleted.transform.position.z), achievCompleted.transform.rotation, achievCompleted.transform);
            newFaja.image.enabled = true;
            importantScript.Book.SetActive(false);
        }

    }
    public static void Sound(string name)
    {
        AudioSource[] audios = Statics.ImportantScript.MainCamera.GetComponentsInChildren<AudioSource>();
        foreach (AudioSource item in audios)
        {
            if (item.name == "Effects")
            {
                item.clip = Resources.Load<AudioClip>(name);
                item.Play();
            }
        }
    }
    public static void Voice(string name)
    {
        AudioSource[] audios = Statics.ImportantScript.MainCamera.GetComponentsInChildren<AudioSource>();
        foreach (AudioSource item in audios)
        {
            if (item.name == "Voice")
            {
                item.clip = Resources.Load<AudioClip>(name);
                item.Play();
            }
        }
    }
    public static void MenuCloseCamera(Vector3 coords, Camera camera)
    {
        camera.transform.position = coords;
    }
}
