﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public int Clock;

    public int Stage = 1;

    public List<Sprite> Fires;

        private void Start()
    {
       Fires = new List<Sprite> { Resources.Load<Sprite>("ognisko1"), Resources.Load<Sprite>("ognisko1"), Resources.Load<Sprite>("ognisko2"), Resources.Load<Sprite>("ognisko3"), Resources.Load<Sprite>("ognisko3"), };
    }

    private void Update()
    {
        Clock++;
        if(Clock > 55)
        {
            Clock = 0;
            Stage++;
            if(Stage > 3)
            {
                Stage = 1;
            }
            gameObject.GetComponentInChildren<SpriteRenderer>().sprite = Fires[Stage];
        }
        //print(Vector3.Distance(Statics.ImportantScript.MainCamera.transform.position, gameObject.transform.position));
        if(Vector3.Distance(Statics.ImportantScript.MainCamera.transform.position,gameObject.transform.position) < 11f)
        {
            gameObject.GetComponentInChildren<AudioSource>().GetComponentInChildren<AudioSource>().enabled = true;
        }
        else
        {
            gameObject.GetComponentInChildren<AudioSource>().GetComponentInChildren<AudioSource>().enabled = false;
        }
    }
    private void OnMouseOver()
    {
        if(Statics.ImportantScript.cursor == 3 && Input.GetMouseButtonDown(0))
        {
            int itemId = Statics.ImportantScript.SelectedItem;
            Functions.Sound("Burn");
            GameObject[] Objects = GameObject.FindObjectsOfType<GameObject>();
            foreach(GameObject item in Objects)
            {
                if(item.name == "Item"+itemId)
                {
                    Functions.ToogleObjectStance(item);
                }
                if(item.name == "Eq"+itemId)
                {
                    Functions.ThrowObject(itemId, item);
                }
            }
            GameObject[] slots = GameObject.FindGameObjectsWithTag("Eq");
            bool FreeInv = true;
            foreach (GameObject item in slots)
            {
                if (item.GetComponentInChildren<EqButton>().ItemPresent)
                {
                    FreeInv = false;
                    //print("Przerwałem ja, " + item);
                    break;
                }
            }
            if (FreeInv)
            {
                Functions.UnlockAchiev("X5");
            }
        }
    }
}
