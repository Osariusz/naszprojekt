﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{

    public int Stage;

    private void Start()
    {
        Stage++;
    }

    private void OnMouseOver()
    {
        Statics.ImportantScript.cursor = 2;
        if(Input.GetMouseButtonUp(0))
        {
            Stage++;
            if (Stage < 5)
            {
                gameObject.GetComponentInChildren<SpriteRenderer>().sprite = Resources.Load<Sprite>("kamien" + Stage);
            }
            if(Stage == 4)
            {
                Functions.UnlockAchiev("X1");
            }
            if(Stage == 10)
            {
                Functions.UnlockAchiev("X6");
            }
            Functions.Sound("RocksFall");
        }
    }
}
