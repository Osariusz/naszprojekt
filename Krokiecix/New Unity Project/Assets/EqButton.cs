﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EqButton : MonoBehaviour
{
    public bool ItemPresent = false;
    ImpScript impScript;
    void Start()
    {
        impScript = Statics.ImportantScript;
    }

    private void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(0) && ItemPresent)
        {
            impScript.cursor = 3;
            Cursor.SetCursor(Resources.Load<Texture2D>(gameObject.name), Vector2.zero, CursorMode.Auto);
            impScript.SelectedItem = (int)gameObject.name[2]-48;
            
        }
        if(Input.GetMouseButtonDown(1) && ItemPresent && gameObject.name == "Eq1")
        {
            impScript.BookOpen = true;
            impScript.Book.SetActive(true);
        }
    }
}
