﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object : MonoBehaviour
{
    public int ObjectId;

    private void Start()
    {
        //Nie ruszać bo trafi do gier z kosza
    }

    private void OnMouseOver()
    {
        if(Statics.ImportantScript.cursor != 3)
        {
            Statics.ImportantScript.cursor = 2;
        }
        if(Input.GetMouseButtonDown(0))
        {
            GameObject[] eqSlots = GameObject.FindGameObjectsWithTag("Eq");
            foreach(GameObject item in eqSlots)
            {
                if(item.name == "Eq"+ObjectId)
                {
                    Functions.ObtainObject(ObjectId,item);
                }
            }
            Statics.ImportantScript.cursor = 0;
            Functions.ToogleObjectStance(gameObject);
        }
        
    }
    private void OnMouseExit()
    {
        if(Statics.ImportantScript.cursor != 3)
        {
            Statics.ImportantScript.cursor = 0;
        }
    }
}
