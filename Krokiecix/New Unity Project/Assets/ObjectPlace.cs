﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPlace : MonoBehaviour
{
    private void OnMouseOver()
    {
        if (Input.GetMouseButtonUp(0) && Statics.ImportantScript.SelectedItem == (int)gameObject.name[1]-48)
        {
            gameObject.GetComponentInChildren<SpriteRenderer>().enabled = true;
            GameObject[] Eqs = GameObject.FindGameObjectsWithTag("Eq");
            foreach(GameObject item in Eqs)
            {
                if(item.name[2] == gameObject.name[1])
                {
                    int id = (int)item.name[2] - 48;
                    Functions.ThrowObject(id,item);
                    Functions.UnlockAchiev("I" + id);
                }
            }
            if(gameObject.name == "P5")
            {
                Functions.Sound("goodHorse");
            }
            if(gameObject.name == "P6")
            {
                Functions.Voice("motyka");
            }
            if(gameObject.name == "P3")
            {
                Functions.Voice("drewno");
            }
            if(gameObject.name == "P2")
            {
                Functions.Voice("ciemno");
            }
        }
        if (Statics.ImportantScript.cursor != 3)
        {
            Statics.ImportantScript.cursor = 2;
        }
    }
    private void OnMouseExit()
    {
        if (Statics.ImportantScript.cursor != 3)
        {
            Statics.ImportantScript.cursor = 0;
        }
    }
}
