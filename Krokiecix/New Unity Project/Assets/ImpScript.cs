﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImpScript : MonoBehaviour
{
    public Camera MainCamera;
    public int cursor;
    public int clicks;
    public int SelectedItem;

    public bool BookOpen = false;
    public bool BookCursor = false;
    public GameObject Book;
    public Button faja;
    public int Page;

    public Canvas Poradnik;
    public Canvas NotPoradnik;

    public bool ClickedRiver = false;
    public bool AwayRiver;
    public GameObject River;

    public Canvas menu;

    public Vector3 oldCoords;

    public bool[] Been = new bool[7];

    public float AchievDelete;

    public bool allAchiev = false;
    public bool DidntShow = true;

    public GameObject NapisyK;

    public float AnotherTimer;

    public void Awake()
    {
        Statics.Importants = gameObject;
        Statics.ImportantScript = gameObject.GetComponentInChildren<ImpScript>();
    }
    public void Start()
    {
        menu.gameObject.SetActive(true);
        if (MainCamera.transform.position != new Vector3(50f, 50f))
        {
            oldCoords = MainCamera.transform.position;
        }
        MainCamera.transform.position = new Vector3(50f, 50f);



        //only to debug

        /*Book.SetActive(true);
        GameObject[] acziwy = GameObject.FindGameObjectsWithTag("Text");
        foreach(GameObject item in acziwy)
        {
            Functions.UnlockAchiev(item.name);
        }
        Book.SetActive(false);*/
    }
    public void Update()
    {
        if(AchievDelete > 0)
        {
            AchievDelete += Time.deltaTime;
        }
        if(AchievDelete > 6f)
        {
            GameObject[] toDelete = GameObject.FindGameObjectsWithTag("AText");
            foreach (GameObject item in toDelete)
            {
                Destroy(item);
            }
            AchievDelete = 0;
        }

        


        allAchiev = true;
        bool IsBookOnline = Book.activeSelf;
        Book.SetActive(true);
        GameObject[] texts = GameObject.FindGameObjectsWithTag("Text");
        foreach (GameObject item in texts)
        {
            if(!item.GetComponentInChildren<AchievData>().Done && item.name != "ignore")
            {
                allAchiev = false;
                break;
            }
        }
        Book.SetActive(IsBookOnline);
        if(allAchiev && DidntShow)
        {
            NapisyK.GetComponentInChildren<Image>().enabled = true;
            DidntShow = false;
            AnotherTimer = 0.1f;
        }
        if(AnotherTimer > 0)
        {
            AnotherTimer += Time.deltaTime;
        }
        if(AnotherTimer > 11f)
        {
            NapisyK.SetActive(false);
            AnotherTimer = 0;
        }
        //print(Input.mousePosition);
        if (cursor != 3)
        {
            Texture2D placeholder = Resources.Load<Texture2D>("Cursor" + cursor);
            Cursor.SetCursor(placeholder, Vector2.zero, CursorMode.Auto);                                                    
        }
        else
        {
            if(Input.GetMouseButtonUp(0))
            {
                if (clicks == 1)
                {
                    cursor = 0;
                    clicks = 0;
                    SelectedItem = 0;
                }
                else if(clicks == 0)
                { clicks = 1; }
            }
        }
        if(Input.GetMouseButtonDown(0) && !BookCursor)
        {
            BookOpen = false;
            Book.SetActive(false);
        }
        if(Vector3.Distance(MainCamera.transform.position,River.transform.position) > 12f)
        {
            AwayRiver = true;
        }
        else
        {
            AwayRiver = false;
        }
        if(AwayRiver && ClickedRiver)
        {
            River.GetComponentInChildren<SingleClicks>().Single = true;
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(menu.gameObject.activeSelf)
            {
                menu.gameObject.SetActive(false);
                print(oldCoords);
                Functions.MenuCloseCamera(oldCoords,MainCamera);
            }
            else
            {
                menu.gameObject.SetActive(true);
                if(MainCamera.transform.position != new Vector3(50f, 50f))
                {
                    oldCoords = MainCamera.transform.position;
                }
                MainCamera.transform.position = new Vector3(50f, 50f);
            }
        }
    }
}

