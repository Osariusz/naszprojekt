﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clicking : MonoBehaviour
{

    Camera mainCamera;

    public int SpecialId;

    private void Start()
    {
        mainCamera = Statics.ImportantScript.MainCamera;
    }
    private void OnMouseOver()
    {
        if(Statics.ImportantScript.cursor != 3)
        {
            Statics.ImportantScript.cursor = 1;
            if (Input.GetMouseButtonUp(0))
            {
                if(Statics.ImportantScript.Poradnik.enabled && Statics.ImportantScript.Poradnik.GetComponentInChildren<AdvisorInfo>().advises.Count > 0)
                {
                    if (Statics.ImportantScript.Poradnik.GetComponentInChildren<AdvisorInfo>().advises.Peek() == 2)
                    {
                        Statics.ImportantScript.Poradnik.GetComponentInChildren<AdvisorInfo>().Frames = 1;
                        Statics.ImportantScript.Poradnik.GetComponentInChildren<AdvisorInfo>().advises.Pop();
                    }
                    if (Statics.ImportantScript.Poradnik.GetComponentInChildren<AdvisorInfo>().AdviseId == 2)
                    {
                        Statics.ImportantScript.Poradnik.GetComponentInChildren<AdvisorInfo>().Frames = 1;
                        Statics.ImportantScript.Poradnik.GetComponentInChildren<AdvisorInfo>().AdviseId = 0;
                        Statics.ImportantScript.Poradnik.GetComponentInChildren<Canvas>().enabled = false;
                    }

                }
                float x = 0;
                float y = 0;
                switch (gameObject.name)
                {
                    case "LeftRoad":

                        x = -20f;
                        break;
                    case "RightRoad":
                        x = 20f;
                        break;
                    case "TurnRoad":
                        x = 120f;
                        break;
                    case "OpTurnRoad":
                        x = -120f;
                        break;
                    case "UpRoad":
                        y = 20f;
                        break;
                    case "DownRoad":
                        y = -20f;
                        break;
                    case "OneRoad":
                        y = -20f;
                        x = -20f;
                        break;
                    case "UnOneRoad":
                        x = 20f;
                        y = 20f;
                        break;
                }
                mainCamera.transform.Translate(new Vector3(x, y, 0), Space.World);
                if(SpecialId == 1)
                {
                    Functions.UnlockAchiev("X2");
                    Functions.Sound("RocksFall");
                    Functions.Voice("au");
                }
                // 2 jest zajęte bo jestem debilem
            }
        }
    }
    public void OnMouseExit()
    {
        if(Statics.ImportantScript.cursor != 3)
        {
            Statics.ImportantScript.cursor = 0;
        }
    }
}
